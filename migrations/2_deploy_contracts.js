/* global artifacts */

const Cracker = artifacts.require('Cracker');
const chalk = require('chalk');
const fs = require('fs');

const seedContract = require('../tools/environment/seed-contract');

function writeConfig(accounts, contractAddress, abi) {
  // console.log('saveConfig()', accounts, contractAddress);
  
  fs.writeFileSync('./src/config.json', JSON.stringify({
    accounts,
    contractAddress,
    abi
  }, null, 4));
}

function log(...args) {
  console.log(...args.map((v) => chalk.green(v)));
}

// -------------------------------------------------------------------------

module.exports = function (deployer, network, accounts) {
  log('Deploying Cracker to:', network);
  const isDev = network !== 'production';
  const deploy = deployer.deploy(Cracker).chain;
  
  deploy.then(() => {
    log(chalk.underline('Accounts'));
    log(...accounts.map((v) => `${v}\n`));
    log('Deployed Cracker at address:', Cracker.address);

    writeConfig(accounts, Cracker.address, Cracker._json.abi);

    // Only seed data for development for now
    if (isDev) {
      seedContract(Cracker.address);
    }
    
    log('deployment complete');
  });
};
