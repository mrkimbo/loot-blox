import { setPrizePot, updateValue } from '../store/contract/actions';
import store from '../store';
import metaMask from '../common/metamask';
import { web3Instance } from "./util";

const getContract = () => {
  return metaMask.contract;
};

const getAccount = () => {
  return store.getState().metaMask.account;
};

const callContract = (method, ...args) => {
  return new Promise((resolve) => {
    getContract()[method](...args, (err, response) => {
      if (err) {
        console.error(`${method}()`, err.message);
        resolve();
        return;
      }
      
      resolve(response);
    });
  });
};

// -------- CONTRACT OPERATIONS --------------------------------

export const init = () => {
  getName();
  getSymbol();
  fetchPot();
};

export const getName = async () => {
  const result = await callContract('NAME');
  
  store.dispatch(updateValue('name', result));
};

export const getSymbol = async () => {
  const result = await callContract('SYMBOL');
  
  store.dispatch(updateValue('symbol', result));
};

export const fetchPot = async () => {
  const amount = await callContract('getPrizePot');
  
  store.dispatch(setPrizePot(amount));
};

export const openBox = async () => {
  const payload = {
    gas: 300000,
    value: web3Instance.toWei(0.1, 'ether'),
    from: getAccount()
  };
  
  try {
    const result = await callContract('openBox', payload);
    store.dispatch(updateValue('processingTransaction', true));
  } catch(err) {}
};

