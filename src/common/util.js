import { ETH_PRECISION } from './constants';
import BigNumber from 'bignumber.js';
import web3 from 'web3';

export const web3Instance = new web3();
export const smashPrice = web3Instance.toWei(0.1, 'ether');

export function truncateAddress(address) {
  const str = address.toLowerCase();
  return str.slice(0,2) === '0x' ? `${str.substr(0, 7)}...` : str;
}

export function formatEth(n) {
  const eth = web3Instance.fromWei(n, 'ether');
  return new BigNumber(eth).toPrecision(ETH_PRECISION);
}

export const pad = (n, len) => {
  const l = n.toString().length;
  return l < len ? `${new Array(len - l).fill(0).join('')}${n}` : n;
};

export const rnd = (min, max) => min + Math.floor(Math.random() * max);
export const rndHex = () => rnd(0, 16).toString(16);
