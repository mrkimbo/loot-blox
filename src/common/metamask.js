import Web3 from 'web3';
import store from '../store/index';
import { updateMetaMask } from '../store/metamask';
import { addContractEvent, updateValue, setPrizePot } from '../store/contract';
import config from '../config.json';

const ADMIN_ACCOUNTS = {
  kim: '0xdb390583babc851e077d7690e45ec63d83c19d81',
  diesel: '0x5c0e6e718ae3f5cfe5e1807d42916f056e2cbad5',
  dom: '0x48a349c1c8b4e5be8a67f04a76994931cccaf418',
};

const TEST_NETS = {
  Morden: 2,
  Ropsten: 3,
  Rinkeby: 4,
  Kovan: 42,
};

const MAX_KNOWN_NETWORK = Math.max(...Object.values(TEST_NETS));

const PING_FREQ = 1000;

export class MetaMask {
  web3 = null;
  filter = null;
  contract = null;
  
  init() {
    // Get/create web3 instance
    if (window.web3) {
      this.web3 = new Web3(window.web3.currentProvider);
    } else {
      this.web3 = new Web3(
        new Web3.providers.HttpProvider('http://127.0.0.1:8545')
      );
    }
    
    this.contract = this.web3.eth.contract(config.abi).at(config.contractAddress);
    this.boxOpenEvent = this.contract.BoxOpen({ }, this.onBoxOpenEvent);
    
    this.ping();
  }
  
  // dispatch regular updates in addition to block-chain updates
  ping = () => {
    store.dispatch(updateMetaMask(this.getInfo()));
    setTimeout(this.ping, PING_FREQ);
  };
  
  checkBalance = () => {
    if (!this.isConnected()) {
      return Promise.resolve(0);
    }
    
    return new Promise((resolve, reject) => {
      this.web3.eth.getBalance(this.getAccount(), (err, balance) => {
        if (err) {
          reject(err);
        } else {
          resolve(this.web3.fromWei(balance.toNumber(), 'ether'));
        }
      });
    });
  };
  
  onBoxOpenEvent = (err, event) => {
    console.log(event.args);
    store.dispatch(addContractEvent(event));
    store.dispatch(setPrizePot(event.args.prizePot));
    store.dispatch(updateValue('processingTransaction', false));
  };
  
  isRunning() {
    return this.web3.currentProvider.isMetaMask;
  }
  
  isConnected() {
    return Boolean(this.getAccount());
  }
  
  getAccount() {
    return this.web3.eth.coinbase;
  }
  
  isMainNet() {
    return this.web3.version.network === 1;
  }
  
  isTestNet() {
    const networkId = this.web3.version.network;
    return networkId > 1 &&
      Object.values(TEST_NETS).includes(networkId);
  }
  
  isLocalNet() {
    return (
      this.web3.version.network > MAX_KNOWN_NETWORK ||
      window.location.hostname === 'localhost'
    );
  }
  
  getAdminAccountName() {
    return Object.keys(ADMIN_ACCOUNTS).find((key) => (
      ADMIN_ACCOUNTS[key] === this.getAccount()
    ));
  }
  
  isAuthorised() {
    // console.log(this);
    return this.isConnected() && (
      Object.values(ADMIN_ACCOUNTS).includes(this.getAccount()) ||
      this.isLocalNet()
    );
  }
  
  getInfo() {
    return {
      running: this.isRunning(),
      connected: this.isConnected(),
      account: this.getAdminAccountName() || this.getAccount() || '',
      authorised: this.isAuthorised(),
    };
  }
  
  destroy() {
    this.filter.stopWatching();
    this.filter = null;
    this.web3 = null;
  }
}

export default new MetaMask();
