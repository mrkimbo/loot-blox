import { ActionTypes } from './actions';
import BigNumber from 'bignumber.js';

const DEFAULT_STATE = {
  name: '',
  symbol: '',
  prizePot: new BigNumber(0),
  processingTransaction: false,
  lastEvent: {
    opener: '',
    prize: new BigNumber(0),
  },
};

export function reducer(state = DEFAULT_STATE, action) {
  const { type, payload } = action;
  
  switch (type) {
    case ActionTypes.ADD_CONTRACT_EVENT:
      return {
        ...state,
        lastEvent: payload.args
      };
    
    case ActionTypes.UPDATE_VALUE:
      return {
        ...state,
        [payload.name]: payload.value,
      };
    
    case ActionTypes.UPDATE_POT:
      return {
        ...state,
        prizePot: payload,
      };
    
    default:
      return { ...state }
    
  }
}
