export const ActionTypes = {
  UPDATE_POT: 'Contract:UpdatePot',
  UPDATE_VALUE: 'Contract:UpdateValue',
  ADD_CONTRACT_EVENT: 'Contract:AddContractEvent',
};

export const updateValue = (name, value) => ({
  type: ActionTypes.UPDATE_VALUE,
  payload: { name, value },
});

export const setPrizePot = (amount) => ({
  type: ActionTypes.UPDATE_POT,
  payload: amount,
});

export const addContractEvent = (event) => ({
  type: ActionTypes.ADD_CONTRACT_EVENT,
  payload: event,
});
