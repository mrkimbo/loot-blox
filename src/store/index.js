import { combineReducers, createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension/logOnlyInProduction';

import * as contract from './contract';
import * as metaMask from './metamask';

export default createStore(
  combineReducers({
    contract: contract.reducer,
    metaMask: metaMask.reducer
  }),
  devToolsEnhancer({ name: 'LootBlox' }),
);
