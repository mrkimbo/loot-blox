
export const ActionTypes = {
  UPDATE: 'MetaMask:Update',
};

export const updateMetaMask = (payload) => ({
  type: ActionTypes.UPDATE,
  payload,
});
