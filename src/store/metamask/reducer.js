import { ActionTypes } from './actionTypes';

const DEFAULT_STATE = {
  connected: false,
  running: false,
  authorised: false,
  contract: null,
  account: '',
};

export function reducer(state = DEFAULT_STATE, action) {
  const { type, payload } = action;
  
  switch (type) {
    case ActionTypes.UPDATE:
      return {
        ...state,
        ...payload
      };
    
    default:
      return { ...state }
    
  }
};
