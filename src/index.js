import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import metaMask from './common/metamask';
import registerServiceWorker from './registerServiceWorker';
import store from './store';
import App from './components/App';

import './common/styles/reset.css';
import './common/styles/global.css';

metaMask.init();

// App entry point
ReactDOM.render(
  <Router>
    <Provider store={store}>
      <App/>
    </Provider>
  </Router>,
  document.getElementById('root')
);

registerServiceWorker();
