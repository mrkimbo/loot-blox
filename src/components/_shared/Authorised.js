import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const Authorised = ({ children, authorised }) => (
  authorised ? children : <Redirect to="/auth" />
);

const mapProps = ({ metaMask }) => ({ authorised: metaMask.authorised });
export default connect(mapProps)(Authorised);
