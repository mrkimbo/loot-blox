import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { truncateAddress } from '../../common/util';
import { connect } from 'react-redux';

import './Header.css';

const EnabledLink = withRouter(({ to, location, children }) => {
  const { pathname } = location;
  return (
    <Link to={to === pathname.slice(1) ? null : to}>
      {children}
    </Link>
  )
});

export const Header = ({ account }) => (
  <header className="header-section">
    <nav>
      <h1 className="header-title">ETHFORTUNΞ</h1>
      <div className="links">
        <EnabledLink to="/home">Home</EnabledLink>
        <EnabledLink to="/faq">FAQ</EnabledLink>
        <div className="account">
          { truncateAddress(account) }
        </div>
      </div>
    </nav>
  </header>
);

const mapStateToProps = ({ metaMask: { account } }) => ({ account });
export default connect(mapStateToProps)(Header);
