import React from 'react';
import { connect } from 'react-redux';

import Authorised from '../_shared/Authorised';
import { init, openBox, fetchPot } from '../../common/contract';
import { formatEth, smashPrice } from '../../common/util';
import Loader from '../../common/assets/loader.gif';

import './Home.css';
import crystal from './crystal-idle.svg';

class Home extends React.Component {
  onButtonClick = () => {
    openBox();
  };
  
  componentWillMount() {
    init();
  }
  
  render() {
    const { prizePot, lastEvent, processingTransaction } = this.props.contract;
    const { prize, opener } = lastEvent;
    
    return (
      <div className="home-section">
        <div className="box-text-area">
          <div className="box-text">
            <p>Smash the gem and you could win an EthFortune! Every smash is a winner. What are you
              waiting for?</p>
            <div className="total-pot">
              <h1>Total Pot Ξ{formatEth(prizePot)}</h1>
            </div>
            <div className="last-win">
              <h2>Last Win Ξ{formatEth(prize)}</h2>
            </div>
          </div>
        </div>
        
        <div className="box-crystal">
          <div className="crystal-area">
            <button className="main-button" onClick={this.onButtonClick}>
              <span>Smash it </span>
              {
                processingTransaction
                  ? <img className="loader" src={Loader} />
                  : <span>Ξ{formatEth(smashPrice)}</span>
              }
            </button>
            <div className="crystal">
              <img src={crystal} alt="crystal"/>
            </div>
          </div>
        </div>
      
      </div>
    );
  }
}

const mapProps = ({ contract, metaMask }) => ({ contract, metaMask });
const ConnectedHome = connect(mapProps)(Home);

export default (props) => (
  <Authorised>
    <ConnectedHome {...props}/>
  </Authorised>
);
