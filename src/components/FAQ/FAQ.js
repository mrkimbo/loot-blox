import React from 'react';
import Authorised from '../_shared/Authorised';

import './FAQ.css';

export class FAQ extends React.Component {
  render() {
    return (
      <div className="faq-section">
        <h2>FAQ</h2>
      </div>
    );
  }
}

export default (props) => (
  <Authorised>
    <FAQ {...props}/>
  </Authorised>
);
