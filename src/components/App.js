import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Header from './_shared';
import Home from './Home';
import FAQ from './FAQ';
import Auth from './Auth';

import './App.css';

const App = () => (
  <div className="App">
    <Header/>
    <div className="main-content">
      <Switch>
        <Route exact path="/home" component={Home}/>
        <Route path="/faq" component={FAQ}/>
        <Route path="/auth" component={Auth}/>
        <Redirect from="*" to="/home"/>
      </Switch>
    </div>
  </div>
);

export default App;
