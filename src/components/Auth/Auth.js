import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import './Auth.css';

export const Auth = ({ authorised }) => {
  if (authorised) {
    return (
      <Redirect to="/home" />
    );
  }
  
  return (
    <div className="auth-section">
      <h2>Private Staging Area</h2>
      <p className="small">
        Connect your MetaMask account to 'Localhost 8545'
        <br/>
        Access is restricted to registered Ethereum addresses
      </p>
    </div>
  );
};

const mapProps = ({ metaMask }) => ({ authorised: metaMask.authorised });
export default connect(mapProps)(Auth);
