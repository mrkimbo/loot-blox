pragma solidity ^0.4.21;


// ToDo: Add reentrancy guards

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';
import 'openzeppelin-solidity/contracts/ownership/Ownable.sol';
import 'openzeppelin-solidity/contracts/ReentrancyGuard.sol';

/**
 * Interaction Notes
 */

contract Cracker is Ownable, ReentrancyGuard {

  // Add prototype methods
  using SafeMath for uint;


  /** --- EVENTS --------------------------------------------------------- **/

  // @dev The BoxOpen event is fired whenever the rock is cracked open
  event BoxOpen(address opener, uint prize, uint prizePot);


  /** --- CONSTANTS ------------------------------------------------------ **/

  // @notice Name and symbol of the token
  string public constant NAME = "LootBlox"; // solhint-disable-line
  string public constant SYMBOL = "LootBloxes"; // solhint-disable-line

  // Random stuff
  uint private constant FACTOR = 1157920892373161954235709850086879078532699846656405640394575840079131296399;

  // Game constants
  uint private constant PLAY_FEE = 0.01 ether;
  uint private constant MIN_POT_SIZE = 0.01 ether;
  uint private constant POT_PERCENTAGE = 50;
  uint private constant TRANSACTION_FEE = 5;
  uint private constant MAX_WIN_PERCENT = 25;

  struct Win {
    address player;
    uint prize;
    uint blockNumber;
    uint timestamp;
  }


  /** --- GLOBAL VARS ---------------------------------------------------- **/

  uint private _prizePot = 1 ether;
  Win[] private _winners;
  Win private _lastWin;

  // @dev Track winnings
  mapping(address => uint) public winningsByUser;
  mapping(address => uint) public winCountByUser;


  /** --- PUBLIC METHODS -------------------------------------------------- **/

  function Cracker() public {
    // Nothing to do here...
  }

  function getPrizePot() public view returns (uint) {
    return _prizePot;
  }

  function getLastWin() public view returns (address player, uint prize, uint blockNumber, uint timestamp) {
    if (_winners.length == 0) {
      return (0, 0, 0, 0);
    }

    return (_lastWin.player, _lastWin.prize, _lastWin.blockNumber, _lastWin.timestamp);
  }

  function getUserStats() public view returns (uint winnings, uint winCount) {
    return (winningsByUser[msg.sender], winCountByUser[msg.sender]);
  }

  // Perform a hack move
  function openBox() public payable {
    //        require(_hasEnoughEther(PLAY_FEE));
    require(_addressIsValid(msg.sender));

    _openBox();
  }

  // !!!! Shit went down! Abort! Abort! !!!!
  function drain() public onlyOwner {
    selfdestruct(owner);
  }

  // Fallback - catch-all method - throw error and refund
  function() public payable {
    revert();
  }

  // --- PRIVATE METHODS -------------------------------------------------- **/

  /**
   * Open the Box
   * Pre-checks:
   *  - Transaction contains enough ether to cover the play fee
   *  - Claimable prize amount exists
   *  - Sender is valid address
   */
  function _openBox() private {
    uint ownerPayout = 0;
    uint senderPayout = 0;
    uint purchaseExcess = 0;
    uint potContribution = 0;
    uint transactionFee = 0;
    uint prize = 0;

    // Calculate fees, winnings, prize pot contribution and overpayment
    purchaseExcess = msg.value.sub(PLAY_FEE);
    potContribution = (PLAY_FEE.div(100)).mul(POT_PERCENTAGE);
    transactionFee = PLAY_FEE.sub(potContribution);

    // Only give prize if pot large enough
    if (_prizePot > MIN_POT_SIZE) {
      prize = _calcPrizeAmount();
    }

    // Apportion out proceeds
    ownerPayout = transactionFee;
    senderPayout = purchaseExcess + prize;
    _prizePot = (_prizePot.sub(prize)).add(potContribution);

    _updateUserStats(prize);

    // Dispatch events
    emit BoxOpen(msg.sender, prize, _prizePot);

    // Execute payments - fees, winnings and excess funds
//    owner.transfer(ownerPayout);
//    msg.sender.transfer(senderPayout);
  }

  /**
   * Calculate random prize amount
   * Caps max prize to percentage of pot
   */
  function _calcPrizeAmount() private view returns (uint) {
    uint winPercent = _generateRandom(MAX_WIN_PERCENT);

    return (_prizePot.div(100)).mul(winPercent);
  }

  function _updateUserStats(uint _prize) private {
    winningsByUser[msg.sender] = winningsByUser[msg.sender].add(_prize);
    winCountByUser[msg.sender] = winCountByUser[msg.sender].add(1);
  }

  /** --- HELPER UTILS ---------------------------------------------------- **/

  /// @dev Ensure claimable prize amount can be won
  function _hasEnoughEther(uint min) private view returns (bool) {
    return msg.value >= min;
  }

  /// @dev Ensure claimable prize amount can be won
  function _prizeCanBeClaimed() private view returns (bool) {
    return _prizePot > 0;
  }

  /// @dev Address safety check to prevent against an unexpected 0x0 default
  function _addressIsValid(address _address) private pure returns (bool) {
    return _address != address(0);
  }

  /// @dev Check for empty string
  function _stringIsEmpty(string _str) private pure returns (bool) {
    bytes memory byteStr = bytes(_str);
    // Uses memory
    return byteStr.length == 0;
  }

  /** --- RANDOM GENERATION ----------------------------------------------- **/

  function _generateRandom(uint max) private view returns (uint) {
    bytes32 rndBytes = _getRandomBytes();
    return uint(rndBytes) % max;
  }

  function _getOffset() public view returns (uint8) {
    return uint8(uint256(sha256(
        keccak256(FACTOR),
        block.difficulty,
        block.coinbase,
        block.timestamp
      )) & 0xff);
  }

  function _getRandomBlockNumber() public view returns (uint) {
    return block.number - _getOffset() - 1;
  }

  function _getRandomBytes() public view returns (bytes32) {
    uint256 blockNumber = _getRandomBlockNumber();
    return keccak256(
      block.number,
      blockhash(blockNumber),
      block.difficulty,
      block.timestamp
    );
  }
}
