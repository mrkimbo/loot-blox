pragma solidity ^0.4.21;


library Random {

    /** --- GLOBAL VARS / CONSTANTS --------------------------------------------- **/

    uint constant private FACTOR =  1157920892373161954235709850086879078532699846656405640394575840079131296399;


    /** --- PRIVATE METHODS ----------------------------------------------------- **/

    function generateRandom(uint max) private view returns (uint) {
        bytes32 rndBytes = _getRandomBytes();
        return uint(rndBytes) % max;
    }

    function _getOffset() public view returns (uint8) {
        return uint8(uint256(sha256(
                keccak256(FACTOR),
                block.difficulty,
                block.coinbase,
                block.timestamp
            )) & 0xff);
    }

    function _getRandomBlockNumber() public view returns (uint) {
        return block.number - _getOffset() - 1;
    }

    function _getRandomBytes() public view returns (bytes32) {
        uint256 blockNumber = _getRandomBlockNumber();
        return keccak256(
            block.number,
            blockhash(blockNumber),
            block.difficulty,
            block.timestamp
        );
    }
}
