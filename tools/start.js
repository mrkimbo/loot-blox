const { inProjectRoot, dispose } = require('./environment/util');
const startContract = require('./environment/contract');
const startUIServer = require('./environment/ui-server');


// NOTE: Assumes script is run from project root (package scripts)
if (!inProjectRoot()) {
  console.error('Start script must be run from package scripts or project root');
  process.exit(1);
}

startContract()
  .then(startUIServer)
  .catch((err) => console.error(err.message));

// Clean up on error or exit --------------------------------------------------------
process.on('exit', dispose.bind(null, { exit: true }));
process.on('SIGINT', dispose);
process.on('SIGINT', dispose);
