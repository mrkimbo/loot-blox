const { runProcess, USING_CLI } = require('./util');

const MNEMONIC = 'host frozen price hub ridge trial milk sight already bunker easy friend';
const GANACHE_PORT = 8545;

// Start ganache --------------------------------------------------------------------
function startGanache() {
  console.log('** START GANACHE-CLI **');
  
  runProcess('ganache-cli', ['-p', GANACHE_PORT, '-m', MNEMONIC, '-a', 2]);
}

// Migrate contract onto local blockchain -------------------------------------------
function deployContract() {
  const network = USING_CLI ? 'development' : 'ganache-ui';
  
  return new Promise((resolve) => {
    const contract = runProcess('truffle', ['migrate', '--network', network]);
    contract.stdout.on('data', (data) => {
      if ((/deployment complete/).test(data.toString())) {
        resolve();
      }
    });
  });
}

module.exports = () => {
  console.log('** START CONTRACT **');
  
  if (USING_CLI) {
    startGanache();
  }
  
  return deployContract();
};
