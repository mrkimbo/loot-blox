const { argv } = require('yargs');
const { spawn } = require('child_process');
const fs = require('fs');

const processes = [];
const USING_CLI = !argv.ui;

function inProjectRoot() {
  return fs.existsSync('./src');
}

// spawn a child process and pipe logs to main stdout
function runProcess(cmd, args = [], opts = {}) {
  const child = spawn(cmd, args, opts);
  child.stdout.on('data', (data) => console.log(data.toString()));
  child.stderr.on('data', (data) => console.error(data.toString()));
  processes.push(child);
  return child;
}

function dispose(options = {}, err = undefined) {
  processes.forEach((pcs) => pcs.kill());
  
  if (err) {
    console.error(err);
  }
  
  if (!options.exit) {
    process.exit();
  }
}

module.exports = {
  runProcess,
  dispose,
  inProjectRoot,
  USING_CLI,
};
