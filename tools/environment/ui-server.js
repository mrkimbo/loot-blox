const { runProcess } = require('./util');

// Start webpack dev server ---------------------------------------------------------

module.exports = () => {
  console.log('** START UI-SERVER **');
  
  runProcess('./node_modules/.bin/react-scripts', ['start']);
};
