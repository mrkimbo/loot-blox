const Web3 = require('web3');
const Contract = require('../../build/contracts/Cracker.json');
const chalk = require('chalk');

let web3;
let contract;

function listAvailableMethods() {
  const blacklist = [
    '_eth', 'abi', 'address', 'bytecode', 'transaction',
    'defaultTxObject', 'filters', 'implements', 'query',
  ];
  const methods = Object.keys(contract).filter((key) => (
    blacklist.every((item) => !new RegExp(item, 'i').test(key))),
  );

  console.log(chalk.green.underline('Available methods:'));
  console.log(...methods.map((v) => `${chalk.yellow(v)}\n`));
}

function connect(address) {
  web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
  contract = web3.eth.contract(Contract.abi).at(address);
}

async function exec() {
  console.log('** SEED CONTRACT **');
  
  // get basic contract info as test
  const contractName = contract.NAME();
  const contractSymbol = contract.SYMBOL();
  const prizePot = contract.getPrizePot().toNumber();
  console.log(chalk.yellow('ContractInfo:'), { contractName, contractSymbol, prizePot });
}

module.exports = function (address) {
  connect(address);
  listAvailableMethods();
  exec();
};
