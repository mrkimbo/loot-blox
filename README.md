### LootBlox - An Ethereum Blockchain game

An unpublished (and unfinished) game running on Ethereum blockchain using a smart contract for the storage and Web3 in the UI.

Player pays fee to take a hack at the loot box. Every turn is taxed a play fee - the remainder goes into the prize pot.
If the prize pot is full, a random prize amount is delivered to the player's eth address.

Random generation not original - forget where I found it, but uses current block properties passed through a few numeric/hashing processors and capped to an upper limit.