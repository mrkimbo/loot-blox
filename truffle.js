module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545, // ganache-cli
      network_id: "*" // Match any network id
    },
    "ganache-ui": {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*" // Match any network id
    },
    testNet: {
    
    }
  }
};
